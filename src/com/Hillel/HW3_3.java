package com.Hillel;

import java.util.Scanner;

public class HW3_3 {
    public static void atb() {

        Scanner sc = new Scanner(System.in);
        System.out.println("Input price :");
        int price = sc.nextInt();
        String res = getPacketInAtb(price);
        System.out.println("Result is " + res);
    }

    private static String getPacketInAtb(int price) {
        switch (price) {
            case 202: return "не хватает 2 grn";
            case 200: return "счастливчик";
            case 100: return "пойду возьму еще что то";

            default:
                return price  + "что то пошло не так.";
        }
    }
}

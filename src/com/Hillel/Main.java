package com.Hillel;

public class Main {
    public static void main(String[] args) {
        //HW3_1.SymbolCombination();
        //HM3_2.UniversityAssessment();
        HW3_3.atb();

//System.out.println("Hello");

  //   boolean ewsult =   love6(2, 4);
  //      System.out.println(ewsult);

   //     int red = redTicket(1, 2, 2);
  //      System.out.println(red);
    }

    //The number 6 is a truly great number. Given two int values, a and b, return true if either one is 6.
    // Or if their sum or difference is 6. Note: the function Math.abs(num) computes the absolute value of a number.
   //    love6(6, 4) → true
   //    love6(4, 5) → false
   //    love6(1, 5) → true

    public static boolean love6(int a, int b) {

        if (a == 6 || b == 6 || a + b == 6 || Math.abs(a - b) == 6)
            return true;

        return false;
    }

    public static int redTicket(int a, int b, int c) {

        int result1 = 10;
        int result2 = 5;
        int result3 = 1;
        if ( a == b && b== c) { //If they are all the value 2, the res is 10.
            if(a == 2)
            {
                return result1;
            }

            return result2;
        }

        if ( a != b  && a != c) { //Otherwise if they are all the same - res is 5.
            return result3;
        }

        return 0;
    }
}
